package WorkWithCircle;

import java.util.Objects;

public class Circle {
    private static final double PI = 3.14;
    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(double radius) {

        this.radius = radius;
    }

    public double getSquare(){
        return PI*(radius*radius);
    }
    public double getPerimeter(){
        return 2*PI*radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}

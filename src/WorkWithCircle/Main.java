package WorkWithCircle;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        List<Circle> list = new ArrayList<>();
        list.add(new Circle(1));
        list.add(new Circle(2));
        list.add(new Circle(3));
        list.add(new Circle(4));
        list.add(new Circle(5));
        list.add(new Circle(3));
        System.out.println("Наибольший по площади");
        Optional<Circle> maxx = list.stream().max(new CircleComparator());
        if(maxx.isPresent()){
            System.out.println(maxx.get().toString());
        }
        System.out.println("Минимальный по площади");
        System.out.println(list.stream().min(new CircleComparator()).get().toString());
        System.out.println("Максимальный по перимтеру");
        System.out.println(list.stream().max(new CircleComparator()).get().toString());
        System.out.println("Минимальный по периметру");
        System.out.println(list.stream().min(new CircleComparator()).get().toString());
    }
}

package WorkWithStudents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Throwable {
        Scanner in = new Scanner(System.in);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
        List<Student> mas = new ArrayList<>();
        mas.add(new Student(1, "Kek", "123", "a", formatter.parse("12 11 1998"), "Улица пушкина", "98987398", "prog", "1pk3", 4));
        mas.add(new Student(2, "wer", "poi", "b", formatter.parse("12 11 1999"), "Дом колотушкина", "98987398", "neprog", "2pk3", 3));
        mas.add(new Student(3, "sdf", "iop", "c", formatter.parse("12 11 1997"), "фыв", "98987398", "prog", "1pk3", 4));
        mas.add(new Student(4, "gcvb", "rty", "d", formatter.parse("12 11 1996"), "Улица ячс", "98987398", "neprog", "2pk3", 2));
        mas.add(new Student(5, "zxc", "dfg", "f", formatter.parse("12 11 1995"), "Улица счмчсм", "98987398", "prog", "2pk3", 2));
        mas.add(new Student(6, "nbnas", "cvb", "g", formatter.parse("12 11 1994"), "Улица ппаапр", "98987398", "prog", "1pk3", 1));
        System.out.println("Вывод студентов заданного круса(ввести номер курса)");
        int course = Integer.parseInt(in.nextLine());
        mas.stream().filter(x -> x.getCourse() == course).forEach(System.out::println);
        ArrayList<String> allFak = new ArrayList<String>();
        allFak.add("prog");
        allFak.add("neprog");
        System.out.println("\nВывод студентов для каждого факультета");
        for (int i = 0; i < allFak.size(); i++) {
            System.out.println("Факульетет: " + allFak.get(i));
            String res = allFak.get(i);
            mas.stream().filter(x -> x.getFak() == res).forEach(System.out::println);
        }
        System.out.println("\nВывод студентов родившихся после заданного года\nВвести дату в формате dd MM yyyy");
        String mej = in.nextLine();
        Date date = formatter.parse(mej);
        mas.stream().filter(x -> x.getDateOf().after(date)).forEach(System.out::println);
        System.out.println("Вывод всех студентов группы(ввести группу(например 1pk3))");
        String group = in.nextLine();
        mas.stream().filter(x -> x.getGroup().equals(group)).forEach(System.out::println);
    }
}

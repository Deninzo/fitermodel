package WorkWithStudents;

import java.util.Date;

public class Student {
    private int id;
    private String secondName;
    private String name;
    private String patronymic;
    private Date dateOf;
    private String adress;
    private String number;
    private String fak;
    private String group;
    private int course;

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOf() {
        return dateOf;
    }

    public void setDateOf(Date dateOf) {
        this.dateOf = dateOf;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFak() {
        return fak;
    }

    public void setFak(String fak) {
        this.fak = fak;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", secondName='" + secondName + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateOf=" + dateOf +
                ", adress='" + adress + '\'' +
                ", number='" + number + '\'' +
                ", fak='" + fak + '\'' +
                ", group='" + group + '\'' +
                ", course=" + course +
                '}';
    }

    public Student(int id, String secondName, String name, String patronymic, Date dateOf, String adress, String number, String fak, String group, int course) {
        this.id = id;
        this.secondName = secondName;
        this.name = name;
        this.patronymic = patronymic;
        this.dateOf = dateOf;
        this.adress = adress;
        this.number = number;
        this.fak = fak;
        this.group = group;
        this.course = course;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

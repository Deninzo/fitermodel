package WorkWithCars;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Throwable {
        final long countmilInYear = 31556952000L;
        List<Car> cars = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        cars.add(new Car(1, "a", "qwe", format.parse("2001"), "Black", 123, "kp123k"));
        cars.add(new Car(2, "b", "zxc", format.parse("2002"), "Black", 133, "kp123k"));
        cars.add(new Car(3, "a", "zxc", format.parse("2003"), "Black", 143, "kp123k"));
        cars.add(new Car(4, "b", "qwe", format.parse("2004"), "Black", 153, "kp123k"));
        cars.add(new Car(5, "c", "qwe", format.parse("2005"), "Black", 163, "kp123k"));
        System.out.println("Вывод машины с выбранной маркой\nВвести марку:");
        String mark = in.nextLine();
        cars.stream().filter(x -> x.getMark().equals(mark))
                .forEach(System.out::println);
        System.out.println("Вывод машин которые эксплатируются больше N лет (n =");
        long countYear = Integer.parseInt(in.nextLine());
        cars.stream().filter(x -> (System.currentTimeMillis() - x.getYear().getTime()) / countmilInYear > countYear)
                .forEach(System.out::println);
        System.out.println("Вывод списока автомобилей заданного года выпуска, цена которых больше указанной(ввести год выпуска и цену)");
        Date year = format.parse(in.nextLine());
        double price = Double.parseDouble(in.nextLine());
        cars.stream().filter(x -> x.getYear().getYear() == year.getYear())
                .filter(x -> x.getPrice() > price)
                .forEach(System.out::println);
    }
}

package WorkWithCustomer;


public class Customer {
    private int id;
    private String name;
    private String secondName;
    private String patronymic;
    private long numberCard;
    private long numberBank;

    public Customer(int id, String name, String secondName, String patronomyc, long numberCard, long numberBank) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.patronymic = patronomyc;
        this.numberCard = numberCard;
        this.numberBank = numberBank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronomyc() {
        return patronymic;
    }

    public void setPatronomyc(String patronomyc) {
        this.patronymic = patronomyc;
    }

    public long getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(long numberCard) {
        this.numberCard = numberCard;
    }

    public long getNumberBank() {
        return numberBank;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", numberCard='" + numberCard + '\'' +
                ", numberBank='" + numberBank + '\'' +
                '}';
    }

    public void setNumberBank(long numberBank) {
        this.numberBank = numberBank;
    }

}

package WorkWithCustomer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        List<Customer> list = new ArrayList<>();
        list.add(new Customer(4, "d", "dd", "ddd", 423123, 612312));
        list.add(new Customer(3, "c", "cc", "ccc", 323123, 512312));
        list.add(new Customer(5, "e", "ee", "eee", 523123, 712312));
        list.add(new Customer(6, "f", "ff", "fff", 623123, 812312));
        list.add(new Customer(1, "a", "aa", "aaa", 123123, 312312));
        list.add(new Customer(2, "b", "bb", "bbb", 223123, 412312));
        System.out.println("Сортировка по алфавиту");
        list.stream().sorted(new CustomerComprator()).forEach(System.out::println);
        System.out.println("Вывод покупателей у который номер кредитной карты в заданном интревале(ввести начало и конец интервала)");
        long start = in.nextLong();
        long end = in.nextLong();
        list.stream().filter(x -> x.getNumberCard() > start && x.getNumberCard() < end).forEach(System.out::println);
    }
}

package WorkWithCustomer;

import java.util.Comparator;

public class CustomerComprator implements Comparator<Customer> {
    @Override
    public int compare(Customer o1, Customer o2) {
        return o1.getName().toUpperCase().compareTo(o2.getName().toUpperCase());
    }
}
